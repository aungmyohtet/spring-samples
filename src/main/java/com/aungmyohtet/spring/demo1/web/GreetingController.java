package com.aungmyohtet.spring.demo1.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aungmyohtet.spring.demo1.entity.Team;
import com.aungmyohtet.spring.demo1.service.TeamService;

@Controller
public class GreetingController {
    
    @Autowired
    private TeamService teamService;

    @GetMapping("/greeting")
    public String greeting(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
        model.addAttribute("name", name);
        return "greeting";
    }
    
    @GetMapping("/login") 
    public String showLoginForm(){
        return "login";
    }
    
    @GetMapping("/teams/create")
    @ResponseBody
    public String createTeam() {
        Team team = new Team();
        team.setName("Team 1");
        teamService.save(team);
        return "success";
    }

}