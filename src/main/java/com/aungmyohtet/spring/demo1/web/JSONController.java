package com.aungmyohtet.spring.demo1.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aungmyohtet.spring.demo1.entity.Team;

@Controller
public class JSONController {

    @PostMapping("/api/teams/create")
    @ResponseBody
    public String postJson(@RequestBody Team team) {
        System.out.println(team.getName());
        Team newTeam = new Team();
        newTeam.setId(6L);
        newTeam.setName(team.getName());
        //return newTeam;
        return "Success";
    }
    
    
}
