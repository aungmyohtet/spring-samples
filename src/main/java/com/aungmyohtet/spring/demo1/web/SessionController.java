package com.aungmyohtet.spring.demo1.web;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aungmyohtet.spring.demo1.service.TeamService;

@Controller
public class SessionController {
    
    @Autowired
    private TeamService teamService;

    @GetMapping("/page1")
    public String getPage1(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
        return "page1";
    }
    
    @PostMapping(value="/page1", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String postPage1(@RequestBody MultiValueMap<String, String> formData, HttpServletRequest request) {
        System.out.println(formData.get("name").get(0));
        request.getSession().setAttribute("name", formData.get("name").get(0));
        return "redirect:/page2";
    }
    
    @GetMapping("/page2")
    public String getPage2(HttpServletRequest request, Model model) {
        model.addAttribute("name", request.getSession().getAttribute("name"));
        return "page2";
    }

}