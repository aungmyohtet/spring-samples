package com.aungmyohtet.spring.demo1.service;

import com.aungmyohtet.spring.demo1.entity.Team;

public interface TeamService {

    void save(Team team);
}
