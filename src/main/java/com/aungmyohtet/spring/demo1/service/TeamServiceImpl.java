package com.aungmyohtet.spring.demo1.service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.aungmyohtet.spring.demo1.entity.Team;

@Service
public class TeamServiceImpl implements TeamService {
    
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional
    public void save(Team team) {
        this.entityManager.persist(team);
    }

}
